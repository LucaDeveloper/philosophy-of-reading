import React, {Component} from 'react';
import './App.css';
import LibraryBuilder from "./containers/LibraryBuilder/LibraryBuilder";

class App extends Component {
    render() {
        return (
            <div className="App">
                <LibraryBuilder/>
            </div>
        );
    }
}

export default App;
